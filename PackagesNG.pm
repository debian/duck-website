# This is basically a copy of libparse-packages-debian-perl, but because of
# Bug #695274 ( http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=695274 )
# I modified the source to make it able to handle mutliple
# multiline-stanzas in Source and Packages files.
#
# What changed is:
#
# Every hash returned is now an Array, even if it holds only 1 line.


use strict;
use lib '.';
package PackagesNG;
our $VERSION = '0.01';

sub new {
    my $class = shift;
    my $fh = shift;

    return bless { fh => $fh }, $class;
}

sub next {
    my $self = shift;
    my $fh   = $self->{fh};
    my %parsed;
    my $lastkey;
    while (<$fh>) {
        last if /^$/;
        if (my ($key, $value) = m/^(\S*): (.*)/) {
	    my @ra;
	    push(@ra,$value);
            $parsed{$key} = \@ra;
	    $lastkey=$key;

        }
        else {
	    if (my ($key) = m/^(\S*):$/)
	    {
		$lastkey=$key;
		my @ra;
		$parsed{$lastkey}=\@ra;
	    }
	    else 
	    {
		s/ //;
		chomp;
		my $ref=$parsed{$lastkey};
		
		my @wa=@{$ref};
		
		if (length($_)>10 )
		{
		    push(@wa,$_);
		}
		$parsed{$lastkey}=\@wa;
		$parsed{body} .= $_;
	    }
        }
    }

    return %parsed;
}

1;


=head1 NAME

Parse::Debian::Packages - parse the data from a debian Packages.gz

=head1 SYNOPSIS

 use YAML;
 use IO::File;
 use Parse::Debian::Packages;
 my $fh = IO::File->new("Packages");

 my $parser = Parse::Debian::Packages->new( $fh );
 while (my %package = $parser->next) {
     print Dump \%package;
 }

=head1 DESCRIPTION

This module parses the Packages files used by the debian package
management tools.

It presents itself as an iterator.  Each call of the ->next method
will return the next package found in the file.

For laziness, we take a filehandle in to the constructor.  Please open
the file for us.

=head1 AUTHOR

Richard Clamp <richardc@unixbeard.net>

=head1 COPYRIGHT

Copyright (C) 2003 Richard Clamp.  All Rights Reserved.

This module is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=head1 SEE ALSO

Module::Packaged

=cut

