#!/usr/bin/perl


use strict;
use warnings;
use Gearman::Worker;
use PackagesNG;
use lib "/usr/share/duck/lib";
use DUCK;
use JSON::XS;
use Data::Dumper;

my $worker = Gearman::Worker->new(job_servers => ['127.0.0.1']);

$worker->register_function(git => \&_git);


our $DUCK= DUCK->new();
our $funcref= $DUCK->cb();

#$DUCK->setOptions("no-https",1);


sub _git {

    my $job = shift;
    my $url = $job->arg;

    my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
    
    my $werte = $coder->decode ($url);
    my $method= $werte->{'method'};
    print "Data in:\n";
#    print Dumper $werte;
    if ($werte->{'forcefail'})
    {
	my $ret;
	my $mock_response;
	$mock_response->{'retval'}=1;
	$mock_response->{'response'}=$werte->{'forcefail_message'};
	$mock_response->{'url'}=$werte->{'url'};

	$ret->{'response'}=$mock_response;
	$ret->{'request'}=$werte; 
	sleep(3);
	return JSON::XS->new->utf8->encode($ret);
    }
    
  
    my $response=&{$funcref->{$method}}($werte->{'url'});
    my $retval=$?;
    print "Data out:\n";
#    print Dumper $response;
    #exit(2);
    my $ret;
 
    $ret->{'response'}=$response;#->{'retval'};
    $ret->{'request'}=$werte;
    print "[".$method."] SP: ".$werte->{'spname'}.", URL: ".$werte->{'url'}."\n";
    sleep(3);

    return JSON::XS->new->utf8->encode($ret);
}




while (1) {
    $worker->work;
}

