#!/usr/bin/perl

#needed packages: libconfig-simple-perl libwww-curl-perl


#use utf8;
#use feature 'unicode_strings';
#no utf8;
use Config::Simple;
use Time::Local;
use HTML::Entities;
use WWW::Curl::Easy;
use Path::Class;
use File::Path qw(make_path remove_tree);
use PackagesNG;
use lib "/usr/share/duck/lib";
use DUCK;
use Data::Dumper;
use strict;
use POSIX qw(strftime);
use HTML::Template;

our $config = new Config::Simple('config');
our $datadir=$config->param("database.datadir");
our $htmldir=$config->param("database.htmldir");
our $templatedir=$config->param("database.templatedir");

our $runid_dates=get_runid_dates();

our $chart_sp_data=$config->param("charts.datafile");
our $chart_outfile=$config->param("charts.outfile");

our $version_cache;

our $maxrunid;
our $minrunid;

our $stats;

my $icons={
    "Vcs-Git" => {
	0 => "GIT_green.png",
	1 => "GIT_red.png"},

    "Vcs-Svn" => {
	0 => "SVN_green.png",
	1 => "SVN_red.png"},

    "Vcs-Darcs" => {
	0 => "DARCS_green.png",
	1 => "DARCS_red.png"},
    
    "Vcs-Hg" => {
	0 => "HG_green.png",
	1 => "HG_red.png"},
    
    "Vcs-Browser" => {
	0 => "BROWSER_green.png",
	1 => "BROWSER_red.png"},
    
    "Homepage" => {
	0 => "HP_green.png",
	1 => "HP_red.png"},

    "Metadata" => {
	0 => "MD_green.png",
	1 => "MD_red.png"}


};

my $icontitle={
    "Vcs-Git" => {
	0 => "GIT:OK",
	1 => "GIT:ERR"},

    "Vcs-Svn" => {
	0 => "SVN:OK",
	1 => "SVN:ERR"},

    "Vcs-Darcs" => {
	0 => "DARCS:OK",
	1 => "DARCS:ERR"},
    
    "Vcs-Hg" => {
	0 => "HG:OK",
	1 => "HG:ERR"},
    
    "Vcs-Browser" => {
	0 => "BROWSER:OK",
	1 => "BROWSER:ERR"},
    
    "Homepage" => {
	0 => "HP:OK",
	1 => "HP:ERR"},

    "Metadata" => {
	0 => "Metadata:OK",
	1 => "Metadata:ERR"}

};

my $workon={
    
    'Vcs-Svn' => {
	'method' => 'Vcs-Svn',
	'dirname' => 'Vcs-Svn',
	'ignore' => 'ssh:\/'    
    },
	    
	    'Vcs-Git' => {
		'method' => 'Vcs-Git',
		'dirname' => 'Vcs-Git'
},
		    'Homepage' => {
			'method' => 'Homepage',
			'dirname' => 'Homepage'
			    
		},
			    'Vcs-Darcs' => {
				'method' => 'Vcs-Darcs',
				'dirname' => 'Vcs-Darcs'
				    
			},
				    'Vcs-Hg' => {
					'method' => 'Vcs-Hg',
					'dirname' => 'Vcs-Hg'
					    
				}
}; 




open(rid,"<runid") or die ("unable to open runid file: $datadir/runid");
my @rid=<rid>;
close(rid);


$maxrunid=$rid[0]+0;
$minrunid=$maxrunid-5;
if ($minrunid<0) {$minrunid=0};
prepare_htmldir();


my $datestring = strftime "%a %b %e %H:%M:%S %Y %z", gmtime;

#create index file
our $number_packages=create_rawfile();
create_indexfile();


my $template_maintainers = HTML::Template->new(filename => $templatedir.'/maintainers.tmpl');
$template_maintainers->param(TIMESTAMP => $datestring);


open(mf,"<$datadir/maintainers.idx");

my @daten=<mf>;
close(mf);

@daten=sort my_sort @daten;

my $current_pref;

my $maintlist;

foreach my $m (@daten)
{
    if ($current_pref ne uc(substr($m,0,1)))
    {
	$current_pref=uc(substr($m,0,1));
	$maintlist.='<h2 style="border-bottom: solid 2px;"><a name="'.$current_pref.'">'.$current_pref.'</a></h2>';
	
    }
    my $maintmail=$m;
    my $linktext;
    if($maintmail =~ m/^(.*)\s*<.*\t(.*)$/) {
	$maintmail=$2;
	$linktext=$1;
    }
    $maintlist.='<a href="maintainer/'.$maintmail.'.html">'.encode_entities($linktext,"<>&").'</a> &lt;'.$maintmail.'&gt;';
    
#############################################################################
# create list of source packages for current maintainer

    my @sps;
    opendir(my $dh, "$datadir/maint/".$maintmail) || die "can't opendir /var/lib: $!";
    @sps = grep { /^[^\.]/ } readdir($dh);
    
    $maintlist.=" (".scalar(@sps).")<br>";
    
    my $template_packages_maint = HTML::Template->new(filename => $templatedir.'/sourcepackages_per_maintainer.tmpl');
    $template_packages_maint->param(TIMESTAMP => $datestring);
    $template_packages_maint->param(MAINTAINER => encode_entities($linktext,"<>&"));
    $template_packages_maint->param(EMAIL => encode_entities($maintmail,"<>&"));
    
    my $packagetable;

    $packagetable=runids_th();
    
    foreach (sort @sps)
    {
	$packagetable.=create_spdataline($_,$maxrunid,$minrunid);
    }
    
    $template_packages_maint->param(PACKAGETABLE => $packagetable);
    make_path("$htmldir/maintainer");
    open (fh_sps,">$htmldir/maintainer/".$maintmail.".html") or die("ahhh");
    print fh_sps $template_packages_maint->output;
    close(fh_sps);
    
}



open(fh_index,">$htmldir/maintainers.html");
$template_maintainers->param(MAINTLIST => $maintlist);
print fh_index $template_maintainers->output;
close(fh_index);


##############################################################################
#create uploaders files

my $template_uploaders = HTML::Template->new(filename => $templatedir.'/uploaders.tmpl');
$template_uploaders->param(TIMESTAMP => $datestring);


open(uf,"<$datadir/uploaders.idx");

my @daten_ul=<uf>;
@daten_ul=sort my_sort @daten_ul;
close(uf);

my $current_pref;

my $ullist;

foreach my $m (@daten_ul)
{
    if (uc($current_pref) ne uc(substr($m,0,1)))
    {
	$current_pref=(substr($m,0,1));
	$ullist.='<h2 style="border-bottom: solid 2px;"><a name="'.$current_pref.'">'.$current_pref.'</a></h2>';
	
    }
    my $ulmail=$m;
    my $linktext;
    if($ulmail =~ m/^(.*)\s*<.*\t(.*)$/) {
	$ulmail=$2;
	$linktext=$1;
    }
    $ullist.='<a href="uploader/'.$ulmail.'.html">'.encode_entities($linktext,"<>&").'</a> &lt;'.$ulmail.'&gt;';
    
# create list of source packages for current uploader

    my @sps;
    opendir(my $dh, "$datadir/uploader/".$ulmail) || die "can't opendir $datadir/uploader/$ulmail: $!,  ul: $m";
    @sps = grep { /^[^\.]/ } readdir($dh);
    
    $ullist.=" (".scalar(@sps).")<br>";
    
    my $template_packages_uploader = HTML::Template->new(filename => $templatedir.'/sourcepackages_per_uploader.tmpl');
    $template_packages_uploader->param(TIMESTAMP => $datestring);
    $template_packages_uploader->param(MAINTAINER => encode_entities($linktext,"<>&"));
    $template_packages_uploader->param(EMAIL => encode_entities($ulmail,"<>&"));
    
    my $packagetable;

    $packagetable.=runids_th();

    foreach (sort @sps)
    {
	$packagetable.=create_spdataline($_,$maxrunid,$minrunid);
    }
    
    $template_packages_uploader->param(PACKAGETABLE => $packagetable);
    make_path("$htmldir/uploader");
    open (fh_sps,">$htmldir/uploader/".$ulmail.".html") or die("ahhh");
    print fh_sps $template_packages_uploader->output;
    close(fh_sps);
    
}

open(fh_index,">$htmldir/uploaders.html");
$template_uploaders->param(UPLOADERLIST => $ullist);
print fh_index $template_uploaders->output;
close(fh_index);


my @checks=keys %$workon;
foreach my $checkindex (@checks)
{
    print "Creating index for check: $checkindex\n";


my @issues;
opendir(my $dh, "$datadir/".$checkindex."/");# || die "can't opendir $datadir/Homepage: $!";
@issues= grep { /^[^\.]/ } readdir($dh);

my $indices;
my $pref;
my $packagelist;
foreach my $iss (sort @issues)
{
    $indices->{substr($iss,0,1)}=1;
    $current_pref=substr($iss,0,1);
    if ($pref ne uc(substr($iss,0,1)))
    {
	$pref=uc(substr($iss,0,1));
	#	  print "<h2>$pref</h2>\n";
	$packagelist.='<div style="border-bottom: solid 2px;"><span style="font-size:1.2em;font-weight:bold;"><br><A name="'.uc($current_pref).'">'.uc($current_pref).'</a></span> [<a title="Navigate to top of page" href="#top">&nbsp;&#x2191;&nbsp;</a>]</div>';
	
    }
    $packagelist.="<a style=\"text-decoration:underline;\" href=\"sp/"._pref($iss)."/$iss".".html"."\">$iss&nbsp;(".get_version($iss).")</a> ";
}

my $template_hp = HTML::Template->new(filename => $templatedir.'/checktemplate.tmpl');
$template_hp->param(TIMESTAMP => $datestring);
$template_hp->param(CHECKNAME => $checkindex);

$template_hp->param(PACKAGELIST => $packagelist);

open (fh_pl,">$htmldir/".$checkindex.".html") or die("ahhh");
print fh_pl $template_hp->output;
close(fh_pl);
}

my @sps;
opendir(my $dh, "$datadir/spname/") || die "can't opendir $datadir/spname: $!";
@sps = grep { /^[^\.]/ } readdir($dh);


#############################################################################
# create package indices

my $indices;
my $pref;
my $packagelist;
foreach my $sp (sort @sps)
{
    $indices->{substr($sp,0,1)}=1;
    $current_pref=substr($sp,0,1);
    #print $sp."\n";
      if ($pref ne uc(substr($sp,0,1)))
      {
	  $pref=uc(substr($sp,0,1));
#	  print "<h2>$pref</h2>\n";
	$packagelist.='<div style="border-bottom: solid 2px;"><span style="font-size:1.2em;font-weight:bold;"><br><A name="'.uc($current_pref).'">'.uc($current_pref).'</a></span> [<a title="Navigate to top of page" href="#top">&nbsp;&#x2191;&nbsp;</a>]</div>';

      }
    $packagelist.="<a style=\"text-decoration:underline;\" href=\"sp/"._pref($sp)."/$sp".".html"."\">$sp&nbsp;(".get_version($sp).")</a> ";
}

my $template_packagelist = HTML::Template->new(filename => $templatedir.'/packagelist.tmpl');
$template_packagelist->param(TIMESTAMP => $datestring);
$template_packagelist->param(PACKAGELIST => $packagelist);

open (fh_pl,">$htmldir/packages.html") or die("ahhh");

print fh_pl $template_packagelist->output;
close(fh_pl);



##############################################################################
#create sourcepackage files

make_path("$htmldir/sp") or die ("Cant create sp subdir!");

foreach my $sp (sort @sps)
{



    my $pref=_pref($sp);
    my $spdir="$htmldir/sp/$pref";
    make_path($spdir);

    my $template_sp = HTML::Template->new(filename => $templatedir.'/sourcepackage.tmpl');
    $template_sp->param(TIMESTAMP => $datestring);

    my $spname=$sp." (".get_version($sp).")";
    $template_sp->param(SPNAME => $spname);
    
    my $template_sp_full = HTML::Template->new(filename => $templatedir.'/sourcepackage_detailed.tmpl');
    $template_sp_full->param(TIMESTAMP => $datestring);
    $template_sp_full->param(SPNAME => $spname);
    
    my $r;
    my $spdata;
    my $spdata_full;
    $spdata.="Jump to: ";
    for ($r=$maxrunid+0;$r>=$minrunid;$r--)
    {
	if (length($runid_dates->{$r}))
	{
	    $spdata.="[<span> <a href=\"#$r\">".$runid_dates->{$r}."</a> </span>]";
	}
    }

    $spdata.='<br>';
    $spdata_full=$spdata;
    for ($r=$maxrunid+0;$r>=$minrunid;$r--)
    {
	my $row.="<hr><span style=\"font-size:1.2em;font-weight:bold;\"><a name=\"$r\">".$runid_dates->{$r}."</a></span> [<a title=\"Navigate to top of page\" href=\"#top\">&nbsp;&#x2191;&nbsp;</a>]";

	$spdata.=$row;
	$spdata.=' [<a href="'.$sp."_full.html#".$r.'">all results</a>] ';

	$spdata_full.=$row;
	$spdata_full.=' [<a href="'.$sp.".html#".$r.'">errors only</a>] ';
	opendir(my $dh,"$datadir/spname/$sp") or die ("unable to open dir $datadir/spname/$sp");
	my @checks= grep { /^[^\.]/ } readdir($dh);
	
	my $mdchecks;
	my $cfchecks;


	foreach my $check ( sort @checks)
	{

	    my $openres=open(fh_result,"<$datadir/spname/$sp/$check/$r");
	    if (!$openres) {next;}
	    my @checkresult=<fh_result>;
	    close(fh_result);

	    $checkresult[0] =~ s/\s+$//;
	    my $class="ok";
	    if ($checkresult[0] ne "0") { $class="crit";}

	    $openres=open(fh_result,"<$datadir/spname/$sp/$check/$r.txt");
	    my @checkoutput=<fh_result>;
	    close(fh_result);


	    my $out=join("",@checkoutput);
	    $out =~ s/\n\n+/\n/g;
	    my $checkresults;
	    $checkresults.="<table class=\"detailed $class\">";
	    $checkresults.="<tr><td>$check <a href=\"".$checkresult[1]."\">".$checkresult[1]."</a></td></tr>";
	    $checkresults.="<tr><td><pre>".encode_entities($out)."</pre></td></tr>";
	    $checkresults.="</table>";

	    if ($checkresult[0] ne "0") 
	    {

	#	$spdata.=$checkresults;
		if ($check =~ m/^Metadata/)
		{
		    $mdchecks->{'err'} .=$checkresults;
		    
		} else
		{
		    $cfchecks->{'err'}.=$checkresults;
		}
		
	    }
	    
	    if ($check =~ m/^Metadata/)
	    {
		$mdchecks->{'full'} .=$checkresults;

	    } else
	    {
		$cfchecks->{'full'}.=$checkresults;
	    }
	    
	}

	if ($cfchecks->{'full'})
	{
	$spdata_full.="<div class=\"controlfile\"><span style=\"font-size:0.8em;\">debian/control:</span><br>";
	$spdata_full.=$cfchecks->{'full'};
	$spdata_full.="</div>";
	}

	if ($mdchecks->{'full'})
	{
	$spdata_full.="<div class=\"metadatafile\"><span style=\"font-size:0.8em;\">upstream-metadata:</span><br>";
	$spdata_full.=$mdchecks->{'full'};
	$spdata_full.="</div>";
	}

	if ($cfchecks->{'err'})
	{
	$spdata.="<div class=\"controlfile\"><span style=\"font-size:0.8em;\">debian/control:</span><br>";
	$spdata.=$cfchecks->{'err'};
	$spdata.="</div>";
	}
	if ($mdchecks->{'err'})
	{
	$spdata.="<div class=\"metadatafile\"><span style=\"font-size:0.8em;\">upstream-metadata:</span><br>";
	$spdata.=$mdchecks->{'err'};
	$spdata.="</div>";
	}


    }

       
    $template_sp->param(TRACKERURL => "https://tracker.debian.org/pkg/$sp");
    $template_sp->param(SPDATA => $spdata);
    $template_sp->param(DETAILS => $sp."_full.html");

    $template_sp_full->param(SPDATA => $spdata_full);
    $template_sp_full->param(ERRORS => $sp.".html");
    $template_sp_full->param(TRACKERURL => "https://tracker.debian.org/pkg/$sp");

    open (fh_sp,">$spdir/$sp.html") or die("ahhh");
    print fh_sp $template_sp->output;
    close(fh_sp);

    open (fh_sp,">$spdir/$sp"."_full.html") or die("ahhh");

    print fh_sp $template_sp_full->output;
    close(fh_sp);


}


# create charts

create_charts();


sub my_sort{

    return lc($a) cmp lc($b);
}



sub create_rawfile()
{
    my $plainfile="sourcepackages.txt";

    opendir(my $dh_plain, "$datadir/spname/") || die "can't opendir /var/lib: $!";
    my @plain = grep { /^[^\.]/ } readdir($dh_plain);

    open(my $pf,">$htmldir/$plainfile");
    print $pf join("\n",sort @plain);
    close($pf);

    return scalar @plain;
}


sub runids_th()
{
    my $res.='<tr><td class="nc"></td>';
    my $r;
    for ($r=$maxrunid;$r>=$minrunid;$r--)
    {
	    $res.="<td style=\"text-align:center; font-weight:bold;border-right:solid 1px;\">".$runid_dates->{$r}."</td>";
    }
    $res.='</tr>';

    return $res;
    
}

sub get_runid_dates()
{
    opendir(my $dh_plain, "$datadir/runids/") || die "can't opendir $datadir/runids: $!";
    my @runids = grep { /^[^\.]/ } readdir($dh_plain);

    my $ret;

    foreach my $rid (@runids)
{

    open(my $rf,"<$datadir/runids/$rid");
    my @rline=<$rf>;
    $rline[0] =~ s/\s+$//;
    $ret->{$rid}=$rline[0];
    close($rf);

}

return $ret;
}

sub create_spdataline()
{
    my $ret;

    my ($spname,$maxrunid,$minrunid)=@_;
    
    my $pref=substr($spname,0,1);
    my $htmlpref=_pref($spname);
    my $path="$datadir/sp/$pref/$spname";

    opendir(my $dh,$path);
    my @checks= grep { /^[^\.]/ } readdir($dh);
    my $i=$maxrunid;

    
    my $v=get_version($spname);
    $ret="<tr><td class=\"nc\"><a href=\"../sp/$htmlpref/$spname.html\">$spname ($v)</a> [<a href=\"https://tracker.debian.org/pkg/$spname\" title=\"Show package information on tracker.debian.org\">&nbsp;T&nbsp;</a>]</td>";
    while ($i>=$minrunid)
    {
	$ret.= "<td class=\"curl\" style=\"border-right:solid 1px;\">";

	my $mdata_state=-1;
	foreach my $check (@checks)
	{

	    my $openres=open(fh_result,"<$path/$check/$i");
	    if (!$openres){ $ret.='&nbsp;'; next};
	    my @checkresult=<fh_result>;
	    close(fh_result);

	    $checkresult[0]  =~ s/\s+$//;
	    my $icon="";
	    my $icon_title="";
	    
	    if ($check =~ m/^Metadata/)
	    {
		if ($mdata_state == -1) 
		{ 
		    $mdata_state=0;
		}

		if ($checkresult[0] ne "0")
		{
		    $mdata_state=1;
		}

	    } else 
	    {
		if ($checkresult[0] ne "0" )
		{
		    $icon=$icons->{$check}->{1};
		    $icon_title=$icontitle->{$check}->{1};
		} else 
		{
		    $icon=$icons->{$check}->{0};
		$icon_title=$icontitle->{$check}->{0};
		    
		}
		
		$ret.= '<img style="vertical-align:middle;" alt="'.$icon_title.'" title="'.$icon_title.'" src="../images/icons/'.$icon.'">';
	    }
	}
	
	if ($mdata_state > -1)
	{
	    my $icon=$icons->{"Metadata"}->{$mdata_state};
	    my $icon_title=$icontitle->{"Metadata"}->{$mdata_state};
#	    print "md: $mdata_state\n";
	    $ret.= '<img style="vertical-align:middle;" alt="'.$icon_title.'" title="'.$icon_title.'" src="../images/icons/'.$icon.'">';	    
	}
	$ret.= "</td>";
	$i--;

    }
    # print $path;
    $ret.="</tr>";
    return $ret;

}

sub prepare_htmldir()
{
# remove output directory

    system ("rm","-rf",$htmldir) ;#or die(" $!");
    system("mkdir","-p",$htmldir);

#copy images and css files to htmldir
    system("cp", "-r", $templatedir."/css", $htmldir) or print "$!\n";
    system("cp", "-r", $templatedir."/images", $htmldir) or print "$!\n";



}

sub create_charts()
{
    if (!$chart_sp_data) { print "No datafile for chart defined, skipping!\n";}


	open(fh_datafile,">$htmldir/".$chart_sp_data);
    
    for (my $c=($maxrunid-99);$c<=$maxrunid;$c++)
    {
	my $rid=$c;
   
	my @v=get_stats("Sourcepackages",$rid);
	chomp @v;
	if ( $v[1] )
	{
	    my @d=split(/-/,$runid_dates->{$rid});
	    my $ts=timelocal(0,0,0,$d[2],$d[1]-1,$d[0]);
	    print fh_datafile $ts."\t ".$v[1]."\n";
	    
	}

    }
	close(fh_datafile);


    my $template_chartfile=HTML::Template->new(filename => $templatedir."/charts/chart.gpi.tmpl");
    $template_chartfile->param(INPUTFILE => $htmldir.'/'.$chart_sp_data);
    $template_chartfile->param(OUTPUTFILE => $htmldir.'/'.$chart_outfile);
    
    
    open(fh_chart,">$htmldir/chart.gpi");
    print fh_chart $template_chartfile->output;
    
    close(fh_chart);

    
    system("gnuplot",$htmldir.'/chart.gpi');
}

sub create_indexfile()
{
    my $template_index = HTML::Template->new(filename => $templatedir.'/index.tmpl');
    $template_index->param(TIMESTAMP => $datestring);

    opendir(my $dh,"$datadir/stats") ;
    my @checks= grep { /^[^\.]/ } readdir($dh);


    my @v=get_stats("Sourcepackages",$maxrunid);
    my $desc=get_stat_description("Sourcepackages");

    my @v_minus1=get_stats("Sourcepackages",$maxrunid-1);
    my $delta;
    if (@v_minus1)
    {
	$delta=$v[1]-$v_minus1[1];
	if ($delta >0) { $delta="+".$delta;}
	$delta="(".$delta.")";
    }
    
    my $pval=sprintf("%.2f",@v[1]*100/@v[0]);
    push (@{$stats}, {KEY => $desc, VALUEERR => @v[1], VALUETOT => @v[0], PERCENT=>" (".$pval." %)", DELTA => $delta} );


    foreach my $c (sort @checks)
    {
	if ($c =~ m/^Metadata|^Sourcepackages/) { next; }
	my @v=get_stats($c,$maxrunid);
	chomp @v;
	my $desc=get_stat_description($c,$maxrunid);
	my $pval="0";
	if (@v[0] == 0 ) { $pval="0";} else
	{
	    $pval=sprintf("%.2f",@v[1]*100/@v[0]);
	}

	my @v_minus1=get_stats($c,$maxrunid-1);
	my $delta;
	if (@v_minus1)
	{
	    $delta=$v[1]-$v_minus1[1];
	if ($delta >0) { $delta="+".$delta;}
	    $delta="(".$delta.")";
	}
	



	push (@{$stats}, {KEY => $desc, VALUEERR => @v[1], VALUETOT => @v[0], PERCENT=>" (".$pval." %)", DELTA => $delta} );


	
    }


    $template_index->param(STATS => $stats);
    open(fh_index,">$htmldir/index.html");
    print fh_index $template_index->output;
    close(fh_index);

    my $template_faq = HTML::Template->new(filename => $templatedir.'/faq.tmpl');
    $template_faq->param(TIMESTAMP => $datestring);

    open(fh_faq,">$htmldir/faq.html");
    print fh_faq $template_faq->output;
    close(fh_faq);



}




sub get_stats()
{
    (my $cn,my $rid)=@_;
    
    if (! -e "$datadir/stats/$cn/$rid")
    {
	return ;
    }

    open (my $fh, "<$datadir/stats/$cn/$rid");
    my @d=<$fh>;
    close($fh);
    
    s/\s*$ // for @d;

    return @d;

}

sub get_version()
{
   my $n=$_[0];
 
    if ($version_cache->{$n}) {return $version_cache->{$n};}
    my $pref=substr($n,0,1);

    if (-e "$datadir/sp/$pref/$n/version")
    {
    open (my $fh, "<$datadir/sp/$pref/$n/version");
    my @d=<$fh>;
	close($fh);
	chomp(@d);
	$d[0] =~ s/\s+$//;

	$version_cache->{$n}=$d[0];
    return $d[0];
	
    } else
    {
	return "";
    }
}

sub get_stat_description()
{
    my $cn=$_[0];

    if (-e "$datadir/stats/$cn/description")
    {
    open (my $fh, "<$datadir/stats/$cn/description");
    my @d=<$fh>;
    close($fh);
    
    return $d[0];

    } else
    {
	return "Issues with $cn";
    }

}



sub _pref()
{
    my $s=$_[0];

    if ( $s =~ m/^lib.+/)
    {
	return substr($s,0,4);
    }
    return substr($s,0,1);
}



exit(0);
