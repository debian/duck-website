#!/usr/bin/perl

#needed packages: libconfig-simple-perl libwww-curl-perl gearman-server duck


use Config::Simple;
use WWW::Curl::Easy;
use Path::Class;
use File::Path qw(make_path remove_tree);
use PackagesNG;
use lib "/usr/share/duck/lib";
use DUCK;
use Data::Dumper;
use Gearman::Client;
use JSON::XS;
use Storable qw(freeze);
use Cwd 'abs_path';

use YAML::XS qw(Load);
use Regexp::Common qw /URI Email::Address/;
use Email::Address;


use strict;

my $config = new Config::Simple('config');

my $sourcesfile=$config->param("mirror.url")."/dists/".$config->param("mirror.dist")."/main/source/Sources.gz";

my $reporting_thresh=$config->param("limits.reporting_threshold");


my $tempdir=abs_path($config->param("database.tempdir"));

my $DUCK= DUCK->new();
my $funcref= $DUCK->cb();


our $spdir;

if ( !(-f "/tmp/duck/Sources") )
{
    make_path("/tmp/duck");
    die("Error downloading Sources file") unless (!downloadfile($sourcesfile,"/tmp/duck/Sources.gz"));
    
    `gunzip -f /tmp/duck/Sources.gz /tmp/duck/Sources`;
}



open(rid,"<runid") or die ("unable to open runid file: ./runid");
my @rid=<rid>;
close(rid);


my $runid=$rid[0]+0;
print "Current Runid:".$runid."\n";

my $datadir=$config->param("database.datadir");

my $fh= IO::File->new("/tmp/duck/Sources");
my $parser =PackagesNG->new( $fh );



my $client  = Gearman::Client->new(job_servers => [$config->param("gearman.server")]);
my $waited  = 0;
my $taskset = $client->new_task_set;      



my $extract_hash;

my @extract=("Homepage",
	     "Repository",
	     "Repository-Browse",
	     "Screenshots",
	     "Bug-Submit",
	     "Bug-Database",
	     "Changelog",
	     "Donation",
	     "FAQ",
	     "Gallery",
	     "Other-References",
	     "Webservice",
	     "Reference",
	     "URL",
	     "Eprint");




my $workon={
    
    'Vcs-Svn' => {
	'method' => 'Vcs-Svn',
	'dirname' => 'Vcs-Svn',
	'ignore' => 'ssh:\/'    
    },
	    
	    'Vcs-Git' => {
		'method' => 'Vcs-Git',
		'dirname' => 'Vcs-Git'
},
		    'Homepage' => {
			'method' => 'Homepage',
			'dirname' => 'Homepage'
			    
		},
			    'Vcs-Darcs' => {
				'method' => 'Vcs-Darcs',
				'dirname' => 'Vcs-Darcs'
				    
			},
				    'Vcs-Hg' => {
					'method' => 'Vcs-Hg',
					'dirname' => 'Vcs-Hg'
					    
				}
}; 




my $counters;

foreach (keys %$workon)
{
    $counters->{$_}->{'total'}=0;
    $counters->{$_}->{'error'}=0;

}

#add key for number of sourcepackages
$counters->{'Sourcepackages'}->{'total'}=0;
$counters->{'Sourcepackages'}->{'error'}=0;


foreach my $a (@extract)
{
    $extract_hash->{$a}=1;
    $counters->{"Metadata-".$a}->{'total'}=0;
    $counters->{"Metadata-".$a}->{'error'}=0;

}

my $reduced_list;

my $lastpackagename;

while (my %package = $parser->next)
{
   my $pname=@{$package{'Package'}}[0];

   my $version=@{$package{'Version'}}[0];

 #  print $pname."\n";
#    $reduced_list->{$pname}->{'versions'}->{$version}=1;

    $reduced_list->{$pname}->{'version'}=$version;

}


$fh->close();
 $fh= IO::File->new("/tmp/duck/Sources");
$parser =PackagesNG->new( $fh );


 $parser =PackagesNG->new( $fh );
#print Dumper $reduced_list;
#die ("aus");
while (my %package = $parser->next)
{
#    my %package=$reduced_list->{$k};

    my $haserror=0;
#    print Dumper %package;
    my $pname=@{$package{'Package'}}[0];

    my $version=@{$package{'Version'}}[0];

    if ($version ne $reduced_list->{$pname}->{'version'})
	{
#	    print "Skipping $pname, version $version\n";
	    next;
	}
#    print "$pname: $version\n";
 
    my $pref=substr($pname,0,1);
 
     $spdir=$datadir."/sp/".$pref."/".$pname;

  

    if ( ! -d $spdir)
    {
	make_path($spdir);
    }

    foreach my $method (keys %$workon)
    {

	if (@{$package{$method}}[0] && (!( -f $spdir."/".$method."/".$runid ) ))
	{
	    my $forcefail=0;
	    my $forcefail_msg;
	    if ($workon->{$method}->{'ignore'})
	    {
		my $re=$workon->{$method}->{'ignore'};
	
		if (@{$package{$method}}[0] =~ m/$re/i) {
		    print "Skipping ".@{$package{$method}}[0].": ".$pname."\n";
		    #next;
		    $forcefail=1;
		    $forcefail_msg="URL is not publicly accessible!!";
		}
	    }
	    
	    my $gitd;
	    $gitd->{'url'}=@{$package{$method}}[0];
	    $gitd->{'spname'}=$pname;
	    $gitd->{'subdir'}=$method;
	    $gitd->{'method'}=$method;
	    if ($forcefail)
	    {
		$gitd->{'forcefail'}=1;
		$gitd->{'forcefail_message'}=$forcefail_msg;;
		
	    }
	    
	    $taskset->add_task(git =>  JSON::XS->new->utf8->encode($gitd), {on_complete => \&handle_git});
	    

	}
	
    }

    my $mdfile=$datadir."/md/".$pref."/".$pname.".upstream";
    if ( -f $mdfile )
    {
	my @yaml_urls;
	
	open my $fh,"<",$mdfile;
	
	my @raw=<$fh>;
	my $raw_string=join("",@raw);
	
	close($fh);
	eval { Load($raw_string);}; if (!$@)
	{
	    my $hashref=Load($raw_string);
	    eval {
		my $dd=scalar keys (%$hashref);
		
	    }; if ($@) { print "skipping $pname\n"; next; }
	    
	    if (!(keys %$hashref)) {next;}
	    
	    foreach my $k (keys %$hashref)
		{
		    if ($extract_hash->{$k})
		    { 
			
			proc("",\@yaml_urls,$k,$hashref->{$k});
			
		    }
		}
		
	}
	
	
	foreach my $y (@yaml_urls)
	{

	    my $gitd;
	    my $m=_guess_type(@{$y}[1]);
	    if (!$m) { print Dumper $y; next;}

	    my $pref=substr($pname,0,1);
	    my $cd=$datadir."/sp/".$pref."/".$pname."/Metadata-".@{$y}[2]."/$runid";
	    if (!(-f abs_path($cd)) ) 
	    {
		$gitd->{'url'}=@{$y}[1];
		$gitd->{'spname'}=$pname;
		$gitd->{'subdir'}="Metadata-".@{$y}[2];
		$gitd->{'method'}=$m;
		$taskset->add_task(git =>  JSON::XS->new->utf8->encode($gitd), {on_complete => \&handle_git});
	    }
	}
	
	


    }
    
}

$fh->close;
print "starting to wait...\n";
$taskset->wait;
print "All workers finished, creating files/symlinks!\n";

 $fh= IO::File->new("/tmp/duck/Sources");
my $parser =PackagesNG->new( $fh );


my @checks=keys (%$workon);


my $mf;
open ($mf, ">$datadir/maintainers.idx");

my $uf;
open ($uf, ">$datadir/uploaders.idx");


my $maintindex;
my $uploadersindex;


#cleanup arena,

remove_tree("$datadir/spname");
make_path("$datadir/spname");

remove_tree("$datadir/maint");
make_path("$datadir/maint");

remove_tree("$datadir/uploader");
make_path("$datadir/uploader");

foreach my $iter_c (@checks)
{
    print "removing dir $iter_c\n";
   remove_tree($datadir."/".$iter_c);
   make_path($datadir."/".$iter_c);
    
}

while (my %package = $parser->next)
{
    $counters->{'Sourcepackages'}->{'total'}++;
    my $haserror=0;

    my $pname=@{$package{'Package'}}[0];
    
    my $version=@{$package{'Version'}}[0];
    my $homepage=@{$package{'Homepage'}}[0];
    my $maint=@{$package{'Maintainer'}}[0];
    
    my $bin=@{$package{'Binary'}}[0];
    my @bin_vals=split(/,\s*/,$bin);
    my $uploaders=@{$package{'Uploaders'}}[0];

    my @uploaders_vals=( $uploaders =~ m/([^>]*>)/g );
    s/^\s*,\s*// for @uploaders_vals;


    my $pref=substr($pname,0,1);

    my $spdir=$datadir."/sp/".$pref."/".$pname;

    opendir(my $dh, "$spdir") || die "can't opendir $spdir $!";
    @checks = grep { /^[^\.]/ } readdir($dh);


    my $check_states;
    
    foreach my $c (@checks)
    {
	my $rid_lower=$runid-6;
	my $rid_results=0;
	my $rid_arr;
	for (my $rid_iter=$runid;$rid_iter>$rid_lower;$rid_iter--)
	{
	    if (-f $spdir."/".$c."/".$rid_iter)
	    {
		my $retval=get_stats($spdir."/".$c."/".$rid_iter);
		$retval =~ s/\s+$//;

		if ($retval ne "0")
		{
		    $rid_results++;
		    $check_states->{$c}=$retval;

		    

		}

	
	    }
	}
	if (-d $spdir."/".$c)
	{
	    $counters->{$c}->{'total'}++;
	}
	
	if ($rid_results >=$reporting_thresh)
	{
	    $haserror=1;
	    $counters->{$c}->{'error'}++;
	    last;
	}

    }

 #   print Data::Dumper $check_states;

    if ($haserror)
    {
	print "Package $pname has errors\n";
	print Dumper $check_states;
	print "\n";

	foreach my $kn (keys %{$check_states})
	{
	    print "creating symlink for check $kn\n";
	    my $symlinkdir=$datadir."/".$kn;
	    make_path($symlinkdir);
	    print "SLD:".$symlinkdir."\n";
	    
	    my $res=symlink(abs_path($datadir."/sp/".$pref."/".$pname),abs_path($symlinkdir."/".$pname));
	    #print abs_path($datadir."/sp/".$pref."/".$pname)."->".abs_path($symlinkdir."/".$pname)."\n";
	    
	}
	
	
	#create version info file
	printf "creatign version info for $pname\n";
	my $f=dir($datadir."/sp/".$pref."/".$pname)->file("version")->openw();
	$f->print($version);
	$f->print("\n");
	$f->close();

	
	my $spnamedir=$datadir."/spname/";
	
	{
	    
	    make_path($spnamedir);
	    my $res=symlink(abs_path($datadir."/sp/".$pref."/".$pname),abs_path($spnamedir."/".$pname));
	    if ($res) {	$counters->{'Sourcepackages'}->{'error'}++;}
	}
	
	
	my $maint_email=$maint;
	if ($maint =~ m/<(.*)>/) 
	{
	    $maint_email=$1;
	}
	
	$maintindex->{$maint_email}=$maint;
	my $maintdir=$datadir."/maint/".$maint_email;
	
	
	make_path($maintdir);
	symlink(abs_path($datadir."/sp/".$pref."/".$pname),abs_path($maintdir."/".$pname));

	foreach my $u (@uploaders_vals)
	{
	    my $ul_email;
	    if($u =~ m/<(.*)>/) 
	    {
		$ul_email=$1;
	    }
	    $uploadersindex->{$ul_email}=$u;
	    my $uldir=$datadir."/uploader/".$ul_email;
	    
	    make_path($uldir);
	    symlink(abs_path($datadir."/sp/".$pref."/".$pname),abs_path($uldir."/".$pname));
	    
	}
	
    }
    
}


foreach my $mk (keys (%$maintindex))
{
    print $mf $maintindex->{$mk}."\t".$mk."\n";
}

close($mf);

foreach my $uk (keys (%$uploadersindex))
{
    print $uf $uploadersindex->{$uk}."\t".$uk."\n";
}

close($uf);


#############################################################################
# Save stats

foreach (keys %$counters)
{
    my $path=$datadir."/stats/".$_;
    make_path($path);
    
    my $openres=open(fh,">$path/$runid");
    
    print fh $counters->{$_}->{'total'}."\n";
    print fh $counters->{$_}->{'error'}."\n";

    close(fh);
}


if (! -e "$datadir/stats/Sourcepackages/description")
{
   open (my $fh, ">$datadir/stats/Sourcepackages/description");  
   print $fh "Number of Sourcepackages with issues\n";
   close($fh);
}


print Dumper $counters;


#-----------------------------------------------------------------------------
# Here be helpers



sub get_stats()
{
    my $cn=$_[0];

    open (my $fh, "<$cn");
    my @d=<$fh>;
    close($fh);
    return @d[0];
}




#############################################################################
sub downloadfile()
{
    my $url=$_[0];
    my $dest=$_[1];
    
    print "Downloading $url to $dest...\n";


my $fh = new IO::File($dest, 'w' ) or die " cannot create new file $!"; 

 my $curl = WWW::Curl::Easy->new;


 $curl->setopt(CURLOPT_HEADER,0);
 $curl->setopt(CURLOPT_SSL_VERIFYPEER,0);
 $curl->setopt(CURLOPT_FOLLOWLOCATION,1);
 $curl->setopt(CURLOPT_TIMEOUT,60);
 $curl->setopt(CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20100101 Firefox/10.0.4 Iceweasel/10.0.4');
 $curl->setopt(CURLOPT_URL, $url);
 
 # A filehandle, reference to a scalar or reference to a typeglob can be used here.

 my $response_body;
 $curl->setopt(CURLOPT_WRITEDATA,\$response_body);

 # Starts the actual request
 my $retcode = $curl->perform;

if ($retcode == 0)
{
    print $fh $response_body;

    undef $fh;
    0;
}
 else 
{
    print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
    undef $fh;
    1;


}

}




sub handle_git()
{
            my $ret = shift;

    	    my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
	    my $werte = $coder->decode ($$ret);

	    my $retval=$werte->{'response'}->{'retval'};
	    my $response=$werte->{'response'}->{'response'};
	    my $url=$werte->{'request'}->{'url'};
	    my $request=$werte->{'request'};
	    my $path=$request->{'subdir'};

	    my $pref=substr($request->{'spname'},0,1);
	    my $spdir=$datadir."/sp/".$pref."/".$request->{'spname'};
	    my $gitdir=$spdir."/".$path."/";
	    make_path($gitdir);# or die("error creating gitdir");;
	    
	    my $f=dir($gitdir)->file($runid)->openw();
	    $f->print($retval);
	    $f->print("\n");
	    $f->print($url);
	    $f->close();
	    
	    $f=dir($gitdir)->file($runid.".txt")->openw();
	    $f->print($response);
	    $f->close();
}


sub proc($;$;$;$;$)
{

    my ($sp,$ref,$key,$r,$p)=@_;
    my $t=ref($r);
    
    if ($t eq "HASH")
    {
	my %a=%{$r};
	foreach my $e (keys %a)
	{
	    proc($sp,$ref,$e,$a{$e},$key);
	}
	
    }
    
    
    if ($t eq "ARRAY")
    {
	
	my @a=@{$r};
	foreach my $e (@a)
	{
	    proc($sp,$ref,$key,$e,$key);
	}
    }
    
    if ($t eq "")
    {
	if ($extract_hash->{$key})
	{
	    my @data=($sp,$r,$key);
	    push(@{$ref},\@data);
	}
    }
}


sub _guess_type($)
{

    my ($url)=@_;
    return "Vcs-Git" if ($url =~/^\s*git:\/\//);
    return "Vcs-Svn" if ($url =~/^\s*svn:\/\//);
    return "URL" if ($url =~/$RE{URI}{HTTP}{-scheme =>'https?'}/);
    return "URL" if ($url =~/$RE{URI}{FTP}/);
    return undef;

}

